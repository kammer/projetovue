import Vue from 'vue'

export const state = () => ({
  auth: null,
  access_level: 'USER',
  carrinho: []
})

export const getters = {
  username(state) {
    return state.auth.user.username
  }
}

export const mutations = {
  SET_AUTH(state, auth) {
    state.auth = auth
  },

  SET_ACCESS_LEVEL(state, level) {
    state.access_level = level
  },

  SET_PRODUCT_IN_CART(state, item) {
    state.carrinho.push(item)
    saveLocalStorageCart(state.carrinho)
  },

  REMOVE_PRODUCT_IN_CART(state, item) {
    console.log(item.name)
    console.log(state.carrinho)
    if (state.carrinho.find(element => element.name === item.name)) {
      state.carrinho.splice(state.carrinho.indexOf(item.name), 1)
    }
    saveLocalStorageCart(state.carrinho)
  },

  SET_CART(state, cart) {
    Vue.set(state, 'carrinho', cart)
  }
}

function saveLocalStorage(credentials) {
  if (process.client) {
    if (credentials != null) {
      localStorage.setItem('loginToken', credentials.auth_token)
    } else {
      localStorage.removeItem('loginToken')
    }
  }
}

function saveLocalStorageCart(cart) {
  if (process.client) {
    if (cart != null) {
      localStorage.setItem('cart', JSON.stringify(cart))
      location.reload()
    } else {
      localStorage.removeItem('cart')
    }
  }
}

export const actions = {
  nuxtClientInit(store, app) {
    store.dispatch('verifyLogin', app)
    if (localStorage.getItem('cart') != null) {
      store.dispatch(
        'updateStoreCart',
        JSON.parse(localStorage.getItem('cart'))
      )
    }
  },
  verifyToken(store, { app, session }) {
    if (!session) {
      console.log('Sem sessão identificada...')
      return
    }

    const headers = {
      Authorization: 'Bearer ' + session
    }

    // Busca as informações no usuário com base no token
    return app.$axios
      .get('http://localhost:8000/auth/users/me/', { headers })
      .then(response => {
        const authData = {
          status: 'success',
          auth_token: session,
          user: response.data
        }

        app.$axios.setToken(session, 'Bearer')
        return store.dispatch('updateLogged', authData).then(_ => {
          const accessInfo = store.state.auth.user.access_level

          if (accessInfo)
            return store.dispatch('setCurrentAccessLevel', accessInfo)
          else return store.dispatch('setCurrentAccessLevel', 'USER')
        })
      })
      .catch(error => {
        console.error('Erro ao armazenar o usuario no Store:', error)
        app.$axios.setToken(false)
        return store
          .dispatch('updateLogged', {})
          .then(_ => store.dispatch('setCurrentAccessLevel', 'USER'))
      })
  },

  verifyLogin({ commit }, app) {
    const currentToken = localStorage.getItem('loginToken')
    if (currentToken != null && currentToken !== 'undefined') {
      this.dispatch('verifyToken', {
        app: app.app,
        session: currentToken
      })
    }
  },

  setCurrentAccessLevel({ commit }, level) {
    commit('SET_ACCESS_LEVEL', level)
  },

  login({ dispatch }, credentials) {
    dispatch('updateLogged', credentials)
    location.reload()
  },

  updateLogged({ commit }, credentials) {
    if (credentials.status === 'success') {
      delete credentials.status
      commit('SET_AUTH', credentials)
      saveLocalStorage(credentials)
    } else {
      commit('SET_AUTH', null)
      saveLocalStorage(credentials)
    }
  },

  updateStoreCart({ commit }, cart) {
    commit('SET_CART', cart)
  },

  updateCart({ commit }, item) {
    if (item.status === 'add') {
      commit('SET_PRODUCT_IN_CART', item.item)
    } else {
      commit('REMOVE_PRODUCT_IN_CART', item.item)
    }
  },

  logout({ commit }) {
    commit('SET_AUTH', null)
    saveLocalStorage(null)
  }
}
