export default function({ store, redirect }) {
  if (store.state.auth != null) return

  redirect('/auth/Login')
}
