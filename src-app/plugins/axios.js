import Vue from 'vue'
import axios from 'axios'

Vue.use(axios)

export default ({ store, isHMR, app: { req, $axios } }) => {
  store.subscribe((mutation, state) => {
    if (mutation.type === 'SET_AUTH' && state.auth != null) {
      $axios.setToken(state.auth.app_session, 'Bearer')
    } else if (state.auth == null) {
      $axios.setToken(false)
    }
  })
}
