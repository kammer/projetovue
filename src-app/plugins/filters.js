import Vue from 'vue'

Vue.filter('tipo', function(value) {
  switch (value) {
    case 1:
      return 'Bebida'
    case 2:
      return 'Verdura'
    case 3:
      return 'Fruta'
    case 4:
      return 'Conveniencias'
    case 5:
      return 'Outros'
  }
})
