from django.db import models

class Produto(models.Model):
    name = models.CharField(max_length=50)
    price = models.FloatField()
    tipo = models.ForeignKey('Tipo', related_name="produtos", on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name