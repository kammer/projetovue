from rest_framework import serializers
from .models import Produto, Tipo

class ProdutoSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Produto
        fields = ('id', 'name', 'price', 'tipo')


class TipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tipo
        fields = ('id', 'name')
