from rest_framework import generics, permissions
from verdureira.models import Produto
from verdureira.serializers import ProdutoSerializer


class ProdutoCreate(generics.CreateAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_class = (

    )


class ProdutoList(generics.ListAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_class = (
    )


class ProdutoDestroy(generics.DestroyAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_classes = (
    )


class ProdutoUpdate(generics.UpdateAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_classes = (
    )


class ProdutoGet(generics.RetrieveAPIView):
    queryset = Produto.objects.all()
    serializer_class = ProdutoSerializer
    permission_classes = ()
