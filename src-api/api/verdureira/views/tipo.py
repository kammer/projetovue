from rest_framework import generics, permissions
from verdureira.models import Tipo
from verdureira.serializers import TipoSerializer

class TipoCreate(generics.CreateAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer
    permission_class = ()


class TipoList(generics.ListAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer
    permission_class = ()


class TipoDestroy(generics.DestroyAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer
    permission_classes = ()


class TipoUpdate(generics.UpdateAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer
    permission_classes = ()


class TipoGet(generics.RetrieveAPIView):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer
    permission_classes = ()