from .produto import ProdutoCreate, ProdutoDestroy, ProdutoGet, ProdutoList, ProdutoUpdate
from .tipo import TipoCreate, TipoDestroy, TipoGet, TipoList, TipoUpdate