from django.conf.urls import url

from .views import ProdutoCreate, ProdutoDestroy, ProdutoGet, ProdutoList, ProdutoUpdate, TipoList, TipoCreate, TipoDestroy, TipoUpdate, TipoGet

urlpatterns = [
    url(r'produtos/$', ProdutoList.as_view()),
    url(r'tipos/$', TipoList.as_view()),

    url(r'produto/add/$', ProdutoCreate.as_view()),
    url(r'tipo/add/$', TipoCreate.as_view()),

    url(r'produto/(?P<pk>\d+)$', ProdutoDestroy.as_view()),
    url(r'tipo/(?P<pk>\d+)$', TipoDestroy.as_view()),

    url(r'produto/edit/(?P<pk>\d+)$', ProdutoUpdate.as_view()),
    url(r'tipo/edit/(?P<pk>\d+)$', TipoUpdate.as_view()),

    url(r'produto/get/(?P<pk>\d+)$', ProdutoGet.as_view()),
    url(r'tipo/get/(?P<pk>\d+)$', TipoGet.as_view()),
]
